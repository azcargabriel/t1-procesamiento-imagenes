#define _DEBUG

// Instruciones:
// Dependiendo de la versi�n de opencv, deben usar los primeros dos includes (cv.h, highgui.h) o bien los dos includes siguientes (imgproc.hpp, highgui.hpp)

//#include <cv.h>
//#include <highgui.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <string>

using namespace std;
using namespace cv;

void convolucion(Mat input, Mat mask, Mat output) {
	for (int r = 0; r < input.rows - mask.rows; r++) {
		for (int c = 0; c < input.cols - mask.cols; c++) {
			int ans = 0;

			//Recorremos la mascara
			for(int m_r = 0; m_r < mask.rows; m_r++) {
				int mm_r = mask.rows - 1 - m_r;

				for(int m_c = 0; m_c < mask.cols; m_c++) {
					int mm_c = mask.cols - 1 - m_c;

					int rr = r + (m_r - int(mask.rows/2));
                    int cc = c + (m_c - int(mask.cols/2));

                    if(rr >= 0 && rr < input.rows && cc >= 0 && cc < input.cols) {
                    	ans += input.at<float>(rr, cc) * mask.at<float>(mm_r, mm_c);
                    }
				}
			}

			//Llenamos el pixel que queda al medio de la mascara
			output.at<float>(r, c) = ans;
		}
	}
}

// No es necesario modificar esta funcion
Mat fft(Mat I)
{
	Mat padded;

	int m = getOptimalDFTSize( I.rows );
    int n = getOptimalDFTSize( I.cols ); // on the border add zero values
    copyMakeBorder(I, padded, 0, m - I.rows, 0, n - I.cols, BORDER_CONSTANT, Scalar::all(0));

	Mat planes[] = {Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F)};
	Mat complexI;

	merge(planes, 2, complexI);
	
	dft(complexI, complexI);
	split(complexI, planes);
	magnitude(planes[0], planes[1], planes[0]);
	Mat magI = planes[0];
	magI += Scalar::all(1);
	log(magI, magI);

	// crop the spectrum, if it has an odd number of rows or columns
	magI = magI(Rect(0, 0, magI.cols & -2, magI.rows & -2));

	// rearrange the quadrants of Fourier image  so that the origin is at the image center
	int cx = magI.cols/2;
	int cy = magI.rows/2;

	Mat q0(magI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
	Mat q1(magI, Rect(cx, 0, cx, cy));  // Top-Right
	Mat q2(magI, Rect(0, cy, cx, cy));  // Bottom-Left
	Mat q3(magI, Rect(cx, cy, cx, cy)); // Bottom-Right

	Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);

	q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
	q2.copyTo(q1);
	tmp.copyTo(q2);

	normalize(magI, magI, 0, 1, CV_MINMAX);
	
	Mat res;
	magI = magI*255;
	magI.convertTo(res, CV_8UC1);
	return res;
}

int computate(int filter, string f_in, bool all) {
	Mat originalRGB;

	originalRGB = imread(f_in); //Leer imagen

	if(originalRGB.empty()) // No encontro la imagen
	{
		cout << "Imagen no encontrada" << endl;
		return 1;
	}

	string file_name = f_in.substr(0, f_in.length() - 4);
	
	Mat original;
	cvtColor(originalRGB, original, CV_BGR2GRAY);
	
	Mat input;
	original.convertTo(input, CV_32FC1);

	// Eleccion de filtro
	cout << "Calculando filtro" << endl;
	Mat mask;

	//Filtro pasa bajo recto
	if(filter == 1) {
		mask = Mat(3, 3, CV_32FC1);

		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				mask.at<float>(i, j) = 1.0/9.0;
		cout << "Filtro pasa bajo recto" << endl;
	}
	//Filtro pasa bajos unidimensional 1x3
	else if (filter == 2) {
		mask = Mat(1, 3, CV_32FC1);

		for(int i = 0; i < 3; i++) {
			mask.at<float>(0, i) = 1.0/3.0;
		}
		cout << "Filtro pasa bajos unidimensional 1x3" << endl;
	}
	//Filtro pasa bajos unidimensional 3x1
	else if (filter == 3) {
		mask = Mat(3, 1, CV_32FC1);
		
		for(int i = 0; i < 3; i++) {
			mask.at<float>(i, 0) = 1.0/3.0;
		}
		cout << "Filtro pasa bajos unidimensional 3x1" << endl;
	}
	//Filtro Gaussiano pasa bajos bidimensional de tama�o 5x5, g=1
	else if(filter == 4) {
		mask = Mat(5, 5, CV_32FC1);
		for(int i = 0; i < 5; i = i + 4) {
			mask.at<float>(i, 0) = 1.0/273;
			mask.at<float>(i, 1) = 4.0/273;
			mask.at<float>(i, 2) = 7.0/273;
			mask.at<float>(i, 3) = 4.0/273;
			mask.at<float>(i, 4) = 1.0/273;
		}

		for(int i = 1; i < 4; i = i+ 2) {
			mask.at<float>(i, 0) = 4.0/273;
			mask.at<float>(i, 1) = 16.0/273;
			mask.at<float>(i, 2) = 26.0/273;
			mask.at<float>(i, 3) = 16.0/273;
			mask.at<float>(i, 4) = 4.0/273;
		}

		mask.at<float>(2, 0) = 7.0/273;
		mask.at<float>(2, 1) = 26.0/273;
		mask.at<float>(2, 2) = 41.0/273;
		mask.at<float>(2, 3) = 26.0/273;
		mask.at<float>(2, 4) = 7.0/273;

		cout << "Filtro Gaussiano pasa bajos bidimensional de tama�o 5x5, g=1" << endl;
	}
	//Filtro Gaussiano pasa bajos unidimensional de tama�o 1x5, g=1
	else if(filter == 5){
		mask = Mat(1, 5, CV_32FC1);
		mask.at<float>(0, 0) = 1.0/17.0;
		mask.at<float>(0, 1) = 4.0/17.0;
		mask.at<float>(0, 2) = 7.0/17.0;
		mask.at<float>(0, 3) = 4.0/17.0;
		mask.at<float>(0, 4) = 1.0/17.0;
		cout << "Filtro Gaussiano pasa bajos unidimensional de tama�o 1x5, g=1" << endl;
	}
	//Filtro Gaussiano pasa bajos unidimensional de tama�o 5x1, g=1
	else if(filter == 6){
		mask = Mat(5, 1, CV_32FC1);
		mask.at<float>(0, 0) = 1.0/17.0;
		mask.at<float>(1, 0) = 4.0/17.0;
		mask.at<float>(2, 0) = 7.0/17.0;
		mask.at<float>(3, 0) = 4.0/17.0;
		mask.at<float>(4, 0) = 1.0/17.0;
		cout << "Filtro Gaussiano pasa bajos unidimensional de tama�o 5x1, g=1" << endl;
	}
	//Filtro pasa altos Prewitt vertical
	else if(filter == 7) {
		mask = Mat(3, 3, CV_32FC1);
		
		for(int i = 0; i < 3; i++) {
			mask.at<float>(i, 0) = -1.0/9.0;
			mask.at<float>(i, 1) = 0;
			mask.at<float>(i, 2) = 1.0/9.0;
		}

		cout << "Filtro pasa altos Prewitt vertical" << endl;
	}
	//Filtro pasa altos Prewitt horizontal
	else if(filter == 8) {
		mask = Mat(3, 3, CV_32FC1);
		
		for(int i = 0; i < 3; i++) {
			mask.at<float>(0, i) = -1.0/9.0;
			mask.at<float>(1, i) = 0;
			mask.at<float>(2, i) = 1.0/9.0;
		}

		cout << "Filtro pasa altos Prewitt horizontal" << endl;
	}
	//Filtro laplaciano 3x3
	else if(filter == 9) {
		mask = Mat(3, 3, CV_32FC1);
		
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				mask.at<float>(i, j) = -1.0/9.0;
		mask.at<float>(1, 1) = 8.0/9.0;

		cout << "Filtro laplaciano 3x3" << endl;
	}
	//Filtro laplaciano 5x5, g=1
	else if(filter == 10) {
		mask = Mat(5, 5, CV_32FC1);

		for(int i = 0; i < 5; i = i + 4) {
			mask.at<float>(i, 0) = 0.0;
			mask.at<float>(i, 1) = 0.0;
			mask.at<float>(i, 2) = -1.0;
			mask.at<float>(i, 3) = 0.0;
			mask.at<float>(i, 4) = 0.0;
		}

		for(int i = 1; i < 4; i = i + 2) {
			mask.at<float>(i, 0) = 0.0;
			mask.at<float>(i, 1) = -1.0;
			mask.at<float>(i, 2) = -2.0;
			mask.at<float>(i, 3) = -1.0;
			mask.at<float>(i, 4) = 0.0;
		}

		mask.at<float>(2, 0) = -1.0;
		mask.at<float>(2, 1) = -2.0;
		mask.at<float>(2, 2) = 16.0;
		mask.at<float>(2, 3) = -2.0;
		mask.at<float>(2, 4) = -1.0;

		cout << "Filtro laplaciano 5x5, g=1" << endl;
	}
	else {
		cout << "Filtro pedido no listado" << endl;
	}


	Mat output = input.clone();	

	cout << "Convolucion..." << endl;
	convolucion(input, mask, output);

	cout << "FFT in..." << endl;
	Mat esp_in;
	esp_in = fft(input);
	imwrite("spectrum_in.jpg", esp_in); // Grabar imagen

	std::stringstream name_spectrum_out;
	name_spectrum_out << filter << "_" << file_name << "_spectrum_out.jpg";
	std::string s_o_name = name_spectrum_out.str();

	cout << "FFT out..." << endl;
	Mat esp_out;
	esp_out = fft(output);
	imwrite(s_o_name, esp_out); // Grabar imagen

	output = abs(output);

	Mat last;
	output.convertTo(last, CV_8UC1);


	std::stringstream name_filtered;
	name_filtered << filter << "_" << file_name << ".jpg";
	std::string f_name = name_filtered.str();
	imwrite(f_name, last); // Grabar imagen

	if(!all) {
		imshow("filtered", last);   // Mostrar imagen
		imshow("spectrum_out", esp_out);
		imshow("spectrum_in", esp_in);
		imshow("original", originalRGB);   // Mostrar imagen
		cvWaitKey(0); // Pausa, permite procesamiento interno de OpenCV
	}
}

int main(int argc, char** argv) {

	if(argc != 3) {
		cout << "Mal formato, requerido: ./conv filtro imagen" << endl;
	}

	string f_in(argv[2]);

	if(atoi(argv[1]) == 0) {
		for(int i = 1; i < 11; i++) {
			computate(i, f_in, true);
		}
	}
	else {
		computate(atoi(argv[1]), f_in, false);
	}
}