#define _DEBUG
#include <iostream>

//#include <cv.h>
//#include <highgui.h>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

float min_value(float a[]) {
	float min = a[0];

	for(int i = 0; i < int(sizeof(&a)/sizeof(float)); i++) {
		if(a[i] < min) {
			min = a[i];
		}
	}
	return min;
}

void ecualizar(Mat input, Mat output) {
	float hist[256];
	for (int i=0; i<256; i++)
		hist[i] = 0;

	//Histograma normalizado
	float v = 1.0 / (input.rows * input.cols);
	for (int r = 0; r < input.rows; r++) {
		for (int c = 0; c < input.cols; c++) {
			int ind = input.at<unsigned char>(r, c);
			hist[ind] = hist[ind] + v;
		}
	}

	//Histograma acumulativo normalizado
	float acc_hist[256];
	acc_hist[0] = hist[0];
	for(int i = 1; i < 256; i++)
		acc_hist[i] = (acc_hist[i - 1] + hist[i]);

	//Se genera la look up table
	float min_v = min_value(acc_hist);
	int look_up_table[256];
	for(int i = 0; i < 256; i++) {
		look_up_table[i] = floor((255 * (acc_hist[i] - min_v) / (1 - min_v)) + 0.5);
	}

	//Llenamos el output
	for (int r = 0; r < input.rows; r++) {
		for (int c = 0; c < input.cols; c++) {
			output.at<unsigned char>(r, c) = look_up_table[input.at<unsigned char>(r, c)];
		}
	}

	return;
}

int main(int argc, char **argv)
{
	Mat originalRGB;
	if(argc == 2) {
		 originalRGB = imread(argv[1]); //Leer imagen
	}
	else {
		cout << "Mal formato, requerido: ./histeq imagen" << endl;
	}

	if(originalRGB.empty()) // No encontro la imagen
	{
		cout << "Imagen no encontrada" << endl;
		return 1;
	}
	
	Mat original;
	cvtColor(originalRGB, original, CV_BGR2GRAY);
	
	Mat output = Mat::zeros(original.rows, original.cols, CV_8UC1);
	ecualizar(original, output);

	imshow("ecualizado", output);   // Mostrar imagen
	imwrite("original.jpg", original);
	imwrite("ecualizado.jpg", output); // Grabar imagen
	cvWaitKey(0); // Pausa, permite procesamiento interno de OpenCV

	return 0; // Sale del programa normalmente
}
